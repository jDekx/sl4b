import {assert, expect} from "chai";
import {FormatterRegister} from "../../src/formatters/FormatterRegister";
import {AbstractFormatter} from "../../src/formatters/AbstractFormatter";
import {LogRecord} from "../../src/logRecord/LogRecord";


suite("FormatterRegister tests", () => {
    test("FormatterRegister namespace should have an object of class FormatterRegister as an export constant", () => {
        const register: FormatterRegister = FormatterRegister.register;
        expect(register).to.be.ok
            .and.to.be.an.instanceOf(FormatterRegister);
    });

    test("Register should resolve character to function, that was registered for this character", () => {
        const register: FormatterRegister = new FormatterRegister();
        const formatter: AbstractFormatter = {
            pattern: "string_pattern",
            format(logRecord: LogRecord): string {
                return "formatter_string";
            }
        };

        register.register("D", formatter)

        expect(register.resolve("d")).to.be.undefined;
        expect(register.resolve("D")).to.be.eq(formatter);

    });

    test("Register should throw an error on registering not single character", () => {
        const formatter: AbstractFormatter = {
            pattern: "string_pattern",
            format(logRecord: LogRecord): string {
                return "formatter_string";
            }
        };

        const register: FormatterRegister = FormatterRegister.register;

        try {
            register.register("not_single_character", formatter)
        } catch(err) {
            expect(err).to.be.an.instanceOf(Error);
            return;
        }

        assert.fail("An Error should be thrown on register call with string as a character pattern parameter.");
    });
});