
import {LogLevel} from "../LogLevel";

/**
 * Class represents single log record, with date, time message and etc. information.
 */
export class LogRecord {
    /**
     * Record date and time.
     */
    public date: Date;

    /**
     * Message log logLevel.
     */
    public logLevel: LogLevel;

    /**
     * Class, method and code line, witch has logged the message.
     */
    public initiator: string;

    /**
     * Log message.
     */
    public message: string;

    /**
     * Formatted message, that will be output to the client.
     */
    public formatted: string;
}