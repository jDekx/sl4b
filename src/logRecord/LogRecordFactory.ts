import {LogRecord} from "./LogRecord";
import {LogLevel} from "../LogLevel";

/**
 * Factory pattern for {@see LogRecord}.
 */
export class LogRecordFactory {

    /**
     * Creates single log record {@see LogRecord} for passed message.
     * @param {string} message A message to be logged.
     */
    public static create(logLevel: LogLevel, message: string) {
        const logRecord: LogRecord = new LogRecord();

        logRecord.date = new Date();
        logRecord.message = message;
        logRecord.logLevel = logLevel;
        //TODO fill log call initiator.
        logRecord.initiator = undefined;
        logRecord.formatted = "";
        return logRecord;
    }

}