import {AbstractFormatter} from "../AbstractFormatter";
import {LogRecord} from "../../logRecord/LogRecord";

/**
 * Formatter for time of log record.
 */
export class TimeFormatter extends AbstractFormatter {

    //TODO fill jsdoc comments and patterns.
    public static readonly FULL: TimeFormatter = new TimeFormatter("");
    public static readonly SHORT: TimeFormatter = new TimeFormatter("");

    /**
     * @constructor
     * @private
     * @inheritDoc
     */
    private constructor(pattern: string) {
        super(pattern);
    };

    /**
     * @inheritDoc
     */
    public format(logRecord: LogRecord): void {
        return null;
    }
}