import {AbstractFormatter} from "../AbstractFormatter";
import {LogRecord} from "../../logRecord/LogRecord";

/**
 * Objects of this class are used to format date in log record.
 */
export class DateFormatter extends AbstractFormatter {

    //TODO fill jsdoc comments and patterns.
    public static readonly FULL: DateFormatter = new DateFormatter("");
    public static readonly SHORT: DateFormatter = new DateFormatter("");

    /**
     * @constructor
     * @private
     * @inheritDoc
     */
    private constructor(pattern: string) {
        super(pattern);
    };

    /**
     * @inheritDoc
     */
    public format(logRecord: LogRecord): void {
        logRecord.formatted += logRecord.date.toString();
    }
}