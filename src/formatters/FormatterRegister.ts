import {AbstractFormatter} from "./AbstractFormatter";
import {DEFAULT_FORMATTERS_MAP} from "./DefaultFormatters";

/**
 * LogRecordFormatter register. Allow classes that implements AbstractFormatter, to be registered for single character.
 * All formatters must be registered with a different characters.
 * When processing log pattern, this register is used to determine formatter for pattern character.
 */
export class FormatterRegister {

    /**
     * Singleton for accessing FormatterRegister.
     * @type {FormatterRegister}
     */
    public static readonly register: FormatterRegister = new FormatterRegister();

    /**
     * Map for holding relation between log pattern character and formatter.
     */
    private formatters: Map<string, AbstractFormatter>;

    /**
     * @constructor
     */
    constructor() {
        this.formatters = new Map<string, AbstractFormatter>();
        this.registerDefaultFormatters();
    }

    /**
     * Register formatter for pattern character.
     * @param {string} patternCharacter character, witch will be replaced by registered formatter output.
     * @param {AbstractFormatterConstructor} formatter formatter to be called to process log record.
     */
    public register(patternCharacter: string, formatter: AbstractFormatter): void {
        if(!patternCharacter.length || patternCharacter.length != 1) {
            throw new Error(`Pattern must be a single character, but got ${patternCharacter}`);
        }

        if(this.formatters.has(patternCharacter)) {
            throw new Error(`Another formatter is already registered for patter ${patternCharacter}.`);
        }

        if(!formatter) {
            throw new Error(`Formatter is required, but got ${formatter}.`);
        }

        this.formatters.set(patternCharacter, formatter);
    }

    /**
     * Resolves pattern character to formatter.
     * @param {string} patternCharacter
     * @returns {AbstractFormatter}
     */
    public resolve(patternCharacter: string): AbstractFormatter {
        return this.formatters.get(patternCharacter);
    }

    /**
     * Register default formatters, provided by this library.
     */
    private registerDefaultFormatters(): void {
        DEFAULT_FORMATTERS_MAP.forEach((value: AbstractFormatter, key: string) => {
            this.register(key, value);
        });
    }
}