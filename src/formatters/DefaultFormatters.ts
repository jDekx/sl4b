
import {AbstractFormatter} from "./AbstractFormatter";
import {DateFormatter} from "./default/DateFormatter";
import {TimeFormatter} from "./default/TimeFormatter";

export const DEFAULT_FORMATTERS_MAP: Map<string, AbstractFormatter> = new Map();

DEFAULT_FORMATTERS_MAP.set("D", DateFormatter.FULL);
DEFAULT_FORMATTERS_MAP.set("d", DateFormatter.SHORT);
DEFAULT_FORMATTERS_MAP.set("T", TimeFormatter.FULL);
DEFAULT_FORMATTERS_MAP.set("t", TimeFormatter.SHORT);

