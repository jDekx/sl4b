import {LogRecord} from "../logRecord/LogRecord";

/**
 * Interface for all formatters, including date formatters, message, type, time, general formatters and etc.
 */
export abstract class AbstractFormatter {

    /**
     * LogRecordFormatter pattern.
     * Literals for this patters defined and use in class that implements this interface.
     */
    pattern: string;

    /**
     * Method for formatting log record.
     * @param {LogRecord} logRecord log record to be formatted.
     */
    public abstract format(logRecord: LogRecord): void;

    public constructor(pattern: string) {
        this.pattern = pattern;
    }
}