import {AbstractFormatter} from "./AbstractFormatter";
import {LogRecord} from "../logRecord/LogRecord";
import {FormatterRegister} from "./FormatterRegister";

export class LogRecordFormatter extends AbstractFormatter{
    //TODO make something more useful, not just SIMPLE.
    public static readonly SIMPLE: LogRecordFormatter = new LogRecordFormatter("D-d-T-t");

    /**
     * @constructor
     * @param {string} pattern
     */
    public constructor(pattern: string) {
        super(pattern);
    }

    /**
     * @inheritDoc
     */
    public format(logRecord: LogRecord): void {
        for(let i = 0; i < this.pattern.length; i++) {
            const char: string = this.pattern.charAt(i);

            const charFormatter: AbstractFormatter = FormatterRegister.register.resolve(char);

            if(charFormatter) {
                //If formatter for current char was registered - use this formatter.
                charFormatter.format(logRecord);
            } else {
                //else just pass character to output message.
                logRecord.formatted += char;
            }
        }
    }
}

