import {LogLevel} from "../LogLevel";
import {LogRecordFactory} from "../logRecord/LogRecordFactory";
import {LogWriter} from "../logWriter/LogWriter";
import {LogRecordFormatter} from "../formatters/LogRecordFormatter";
import {LogRecord} from "../logRecord/LogRecord";

export class Logger {

    /**
     * Flag, determines process debug calls.
     */
    private debugMode: boolean;

    /**
     * Log writer for outputting log messages.
     */
    private logWriter: LogWriter;

    /**
     * Formatter for log record processing.
     */
    private formatter: LogRecordFormatter;

    //TODO add jsdoc comment.
    public constructor(formatter: LogRecordFormatter, logWriter: LogWriter, enableDebugMode?: boolean) {
        this.formatter = formatter;
        this.logWriter = logWriter;
        this.debugMode = enableDebugMode || false;
    }

    /**
     * Log messages with default (info) log logLevel.
     * Several messages as varargs can be passed in this method.
     * Every single message will be logged with separated log record.
     */
    public log(...messages: string[]):void {
        this.info(...messages);
    }

    /**
     * Log messages with info log logLevel.
     */
    public info(...messages: string[]): void {
        this.processLog(LogLevel.INFO, ...messages);
    }

    /**
     * Log messages with warning log logLevel.
     */
    public warning(...messages: string[]): void {
        this.processLog(LogLevel.WARNING, ...messages);
    }

    /**
     * Log messages with error log logLevel.
     */
    public error(...messages: string[]): void {
        this.processLog(LogLevel.ERROR, ...messages);
    }

    /**
     * Log messages with debug log logLevel, if {@see debugMode} is enabled.
     */
    public debug(...messages: string[]): void {
        if(!this.debugMode) {
            return;
        }

        this.processLog(LogLevel.DEBUG, ...messages);
    }

    /**
     * Formats and outputs messages.
     */
    private processLog(logLevel: LogLevel, ...messages: string[]) {
        const formattedLogRecords: string[] = [];

        messages.forEach((message: string) =>  {
            const logRecord: LogRecord = LogRecordFactory.create(logLevel, message);
             this.formatter.format(logRecord);
            formattedLogRecords.push(logRecord.formatted);
        });

        this.logWriter.writeArray(formattedLogRecords);
    }
}