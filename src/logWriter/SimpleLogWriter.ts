import {LogWriter} from "./LogWriter";

/**
 * Simple class, implementing LogWriter, for output messages into browser, using console calls.
 */
//TODO support different browsers and node.js server console.
export class SimpleLogWriter implements LogWriter {

    /**
     * @inheritDoc
     */
    write(formattedLogRecord: string): void {
        console.log(formattedLogRecord);
    }

    /**
     * @inheritDoc
     */
    writeArray(formattedLogRecords: string[]): void {
        formattedLogRecords.forEach((output: string) => {
            this.write(output);
        });
    }
}