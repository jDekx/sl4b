/**
 * Interface for classes to output messages using browser console, system or other ways.
 */
export interface LogWriter {

    /**
     * Output single log record.
     * @param {string} formattedLogRecord formatted record to be written.
     */
    write(formattedLogRecord: string): void;

    /**
     * Output multiple log records.
     * @param {string[]} formattedLogRecords array of records to be written.
     */
    writeArray(formattedLogRecords: string[]): void;
}