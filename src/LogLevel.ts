/**
 * Enum represents loggin levels.
 */
export enum LogLevel {
    /**
     * Information messages.
     */
    INFO = 0,

    /**
     * Warning messages.
     */
    WARNING = 1,

    /**
     * Error messages.
      */
    ERROR = 2,

    /**
     * Debugging messages.
     */
    DEBUG = 3,

    /**
     * All messages.
     */
    ALL = 4
}