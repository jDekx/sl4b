import {Logger} from "./logger/Logger";
import {LogRecordFormatter} from "./formatters/LogRecordFormatter";
import {SimpleLogWriter} from "./logWriter/SimpleLogWriter";

const logger: Logger = new Logger(LogRecordFormatter.SIMPLE, new SimpleLogWriter());

export function getLogger() {
    return logger;
}