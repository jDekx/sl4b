const gulp = require("gulp");
const path = require("path");
const ts = require("gulp-typescript");
const del = require("del");
const tsProject = ts.createProject("tsconfig.json");
const browserSync = require("browser-sync").create();
const mocha = require("gulp-mocha");

const DIST = path.join(__dirname, "dist");
const SRC = path.join(__dirname, "src");
const TESTS = path.join(__dirname, "test");
//TODO remove ts from being copy for source map.
const STATIC = ["png", "ico", "jpeg", "html", "js", "js.map", "ts"];

function makeGlobForStatic() {
	return "/**/*.{" + STATIC.join(",") + "}";
}

gulp.task("ts:source", function() {
	return gulp.src(SRC + "/**/*.ts")
		.pipe(tsProject())
		.pipe(gulp.dest(SRC));
});

gulp.task("ts:tests", function() {
    return gulp.src(TESTS + "/**/*.ts")
        .pipe(tsProject())
        .pipe(gulp.dest(TESTS));
});

gulp.task("copy:static", function() {
	return gulp.src(SRC + makeGlobForStatic())
	.pipe(gulp.dest(DIST))
});

gulp.task("clean", function(){
	return del("./dist");
});

gulp.task("build", function() {
	gulp.start("ts:source");
	gulp.start("copy:static");
    gulp.start("copy:rjs");
    gulp.start("copy:pixi.js");
});

gulp.task("copy:rjs", function() {
	return gulp.src("./node_modules/requirejs/require.js")
		.pipe(gulp.dest(DIST));
});

gulp.task("rebuild", ["clean"], function(){
	gulp.start("build");
});

gulp.task("watch", ["build"], function(){
    gulp.watch(SRC + makeGlobForStatic(), ["copy:static", browserSync.reload]);
});

gulp.task("server", function(){
    browserSync.init({
        server: DIST
    });

    browserSync.watch(DIST + "/**/*.*").on("change", browserSync.reload);
});

gulp.task("default", ["rebuild"], function() {
	// gulp.start("watch");
});

gulp.task("test", ["ts:tests"], function (done) {
    gulp.src("test.js", {read: false})
    	// `gulp-mocha` needs filepaths so you can"t have any plugins before it
        .pipe(mocha({
        	ui: "tdd",
                recursive: true

        }))
});

